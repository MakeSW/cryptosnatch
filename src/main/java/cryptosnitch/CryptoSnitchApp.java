package cryptosnitch;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import cryptosnitch.bitcoin.AddressReader;
import cryptosnitch.bitcoin.ShutdownableRunnable;
import cryptosnitch.bitcoin.WalletGeneratorThread;
import cryptosnitch.bitcoin.WalletSink;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

public class CryptoSnitchApp {
	private static final int DEFAULT_WORKER_THREAD_COUNT = 5;
	
	private static final int DEFAULT_CONSUMER_THREAD_COUNT = 1;
	
	private final int workerThreadCount;
	
	private final ShutdownableRunnable[] threads;
	
	private final ExecutorService service;
	
	private final Set<String> addresses;
	
	private boolean isRunning = false;
	
	private final int consumerThreadCount;

	public CryptoSnitchApp(int workerThreadCount, int consumerThreadCount, String addressFilePath) throws IOException {
		AddressReader addressReader = new AddressReader(addressFilePath);
		
		this.consumerThreadCount = consumerThreadCount;
		this.workerThreadCount = workerThreadCount;
		this.threads = new ShutdownableRunnable[workerThreadCount + consumerThreadCount];
		this.service = Executors.newFixedThreadPool(workerThreadCount + consumerThreadCount);
		this.addresses = addressReader.getAsSet();
		
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			public void run() {
				shutdown();
			}
		}));
	}
	
	public void start() throws FileNotFoundException, InterruptedException {
		this.isRunning = true;
		
		System.out.println(String.format("Initialized address cache with %d entries", this.addresses.size()));
		
		for (int i = 0; i < this.consumerThreadCount; i++) {
			WalletSink sink = new WalletSink(i + 1, this.addresses);
			sink.start();
			this.threads[i] = sink;
			this.service.execute(sink);
		}
		
		for (int i = 0; i < this.workerThreadCount; i++) {
			WalletGeneratorThread t = new WalletGeneratorThread(i + 1, WalletSink.getQueue());
			t.start();
			this.threads[i + this.consumerThreadCount] = t;
			this.service.execute(t);
		}
		
		while (isRunning) {			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				/* Ignore */
			}
		}
	}
	
	public static void main(String[] args) throws NoSuchAlgorithmException, InvalidAlgorithmParameterException, InterruptedException, IOException {
		ArgumentParser parser = ArgumentParsers.newFor("cryptosnitch").build()
					.defaultHelp(true)
					.description("Uses random generator to create Bitcoin wallets in the form of tripples (private key, public key, address");
		parser.addArgument("-w", "--worker-count")
			.type(Integer.class)
			.setDefault(DEFAULT_WORKER_THREAD_COUNT)
			.help("Sets the number of worker threads generating wallets");
		parser.addArgument("-c", "--consumer-count")
			.type(Integer.class)
			.setDefault(DEFAULT_CONSUMER_THREAD_COUNT)
			.help("Sets the number of address checker threads");
		parser.addArgument("-a", "--addresses-path")
			.type(String.class)
			.help("Defines the path to the file containig addresses for checking matches (mandatory)");
		
		
		try {
			Namespace res = parser.parseArgs(args);
			
			String addressPath = res.getString("addresses_path");
			Integer workerCount = res.getInt("worker_count");
			Integer consumerCount = res.getInt("consumer_count");
			
			if (addressPath == null) {
				parser.printHelp();
				System.exit(1);
				return;
			}
			
			CryptoSnitchApp app = new CryptoSnitchApp(workerCount, consumerCount, addressPath);
			app.start();
		} catch (ArgumentParserException e) {
			parser.handleError(e);
			System.exit(1);
		}
	}
	
	private void shutdown() {
		System.out.println("Shutting down...");
		
		this.isRunning = false;
		
		for (int i = 0; i < this.threads.length; i++) {
			System.out.printf("   shutting down thread %d\n", i + 1);
			ShutdownableRunnable r = this.threads[i];
			r.shutdown();
		}
		
		System.out.println("   waiting 3sec for threads to complete shutdown");
		
		try {
			service.shutdown();
			service.awaitTermination(3, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			System.out.println("...failed to wait for thread termination");
		}
		
		System.out.println("Completed shutdown procedure");
	}
}
