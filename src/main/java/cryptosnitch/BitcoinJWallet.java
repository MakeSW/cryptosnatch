package cryptosnitch;

import java.util.Calendar;
import java.util.stream.Collectors;

import org.bitcoinj.core.Address;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.script.Script.ScriptType;
import org.bitcoinj.wallet.Wallet;

public class BitcoinJWallet {

	public static void main(String[] args) {
		
		NetworkParameters p = new MainNetParams();
		
		for (int i = 0; i < 1000; i++) {	
			long start = Calendar.getInstance().getTimeInMillis();
			long addrDuration = 0;

			Wallet wallet = Wallet.createDeterministic(p, ScriptType.P2PKH);
			
			System.out.printf("Script type: %s", ScriptType.P2PKH .name());
			
			long addrStart = Calendar.getInstance().getTimeInMillis();
			
			Address addr = wallet.freshReceiveAddress(ScriptType.P2PKH );
			
			long addrEnd = Calendar.getInstance().getTimeInMillis();
			addrDuration += (addrEnd - addrStart);
			
			System.out.printf("Generating %s address %s took %dms\n", ScriptType.P2PKH.name(), addr.toString(), addrEnd - addrStart);
			
			System.out.printf("seed: %s\n\n", wallet.getKeyChainSeed().getMnemonicCode().stream().collect(Collectors.joining(" ")));
			
			long end = Calendar.getInstance().getTimeInMillis();
			System.out.printf("Generated wallet and addresses in %dms\n", end - start);
			System.out.printf(" ..wallet alone %dms\n", end - start - addrDuration);
		}
		
	}

}
