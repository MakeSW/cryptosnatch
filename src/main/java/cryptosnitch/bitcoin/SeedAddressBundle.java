package cryptosnitch.bitcoin;

public class SeedAddressBundle {
	private final String seedPhrase;
	private final String[] addresses;
	
	public SeedAddressBundle(String seedPhrase, String[] addresses) {
		this.seedPhrase = seedPhrase;
		this.addresses = addresses;
	}
	
	public String[] getAddresses() {
		return addresses;
	}

	public String getSeedPhrase() {
		return seedPhrase;
	}
}
