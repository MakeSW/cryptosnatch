package cryptosnitch.bitcoin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class AddressReader {
	private final File file;
	
	public AddressReader(String path) {
		this.file = new File(path);
	}
	
	public Set<String> getAsSet() throws IOException {
		final Set<String> set = new HashSet<String>();
		final FileReader fileReader = new FileReader(file);
		final BufferedReader reader = new BufferedReader(fileReader);
		String line = null;

		do {
			line = reader.readLine();
			
			if (line != null) {
				set.add(line.trim());
			}
		} while(line != null);
		
		reader.close();
		
		return set;
	}
}
