package cryptosnitch.bitcoin;

public interface ShutdownableRunnable extends Runnable {
	void shutdown();
}
