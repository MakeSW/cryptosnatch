package cryptosnitch.bitcoin;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;

public class WalletSink implements ShutdownableRunnable {
	private static final int MAX_SIZE = 100000;

	private static final ArrayBlockingQueue<SeedAddressBundle> queue = new ArrayBlockingQueue<SeedAddressBundle>(WalletSink.MAX_SIZE);
	
	private static final int BATCH_SIZE = 100;
	
	private static final String DEFAULT_FILE_NAME = "checkpot_wallets.csv";
		
	private final Set<String> knownAddressesSet;
	
	private final String outputPath;
	
	private boolean isRunning = false;
	
	private final BufferedWriter writer;
	
	private final int id;
	
	public WalletSink(int id, Set<String> knownAddresses, String outputPath) throws FileNotFoundException {
		this.id = id;
		this.knownAddressesSet = knownAddresses;
		this.outputPath = outputPath;
		FileOutputStream output = new FileOutputStream(new File(this.outputPath), true);
		this.writer = new BufferedWriter(new OutputStreamWriter(output));

	}
	
	public WalletSink(int id, Set<String> knownAddressesSet) throws FileNotFoundException {
		this(id, knownAddressesSet, DEFAULT_FILE_NAME);
	}
	
	public void start() {
		System.out.printf("Starting consumer thread %d\n", this.id);
		this.isRunning = true;
	}
	
	public void run() {
		List<SeedAddressBundle> walletBatch = new ArrayList<SeedAddressBundle>();

		long walletCount = 0;
		long addressCount = 0;
		
		long start = Calendar.getInstance().getTimeInMillis();
		while (isRunning) {			
			walletCount += WalletSink.queue.drainTo(walletBatch);
						
			for (SeedAddressBundle seedBundle : walletBatch) {	
				String[] addresses = seedBundle.getAddresses();
				addressCount += addresses.length;
				
				for (String address : addresses) {					
					if (this.knownAddressesSet.contains(address)) {
						persistFound(seedBundle.getSeedPhrase(), address);
					}
				}					
			}
			
			if (walletCount >= (BATCH_SIZE * 10)) { 
				long end = Calendar.getInstance().getTimeInMillis();
				float timeInMs = end - start;
				float walletsPerSec = ((float)walletCount) / timeInMs * 1000;
				
				System.out.printf("Generation of %d wallets (%d addresses) took %f sec (%f wallets/sec)\n",
						walletCount,
						addressCount,
						timeInMs / 1000.0,
						walletsPerSec);

				walletCount = 0;
				addressCount = 0;
				start = Calendar.getInstance().getTimeInMillis();
			}
		}
	}
	
	private void persistFound(String seedPhrase, String address) {
		String line = String.format("%s;%s", seedPhrase, address);
		System.out.println("Match found!");
		System.out.println(line);
		
		try {
			writer.append(line);
			writer.flush();
		} catch (IOException e) {
			System.err.println("Could not persist wallet data");
			e.printStackTrace(System.err);
		}
	}

	public static ArrayBlockingQueue<SeedAddressBundle> getQueue() {
		return WalletSink.queue;
	}

	public void shutdown() {
		try {
			this.writer.close();			
		} catch (IOException e) {
			System.err.println("Could not close writer");
			e.printStackTrace(System.err);
		}
		this.isRunning = false;
	}
}
