package cryptosnitch.bitcoin;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.stream.Collectors;

import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.script.Script.ScriptType;
import org.bitcoinj.wallet.Wallet;

public class WalletGeneratorThread implements ShutdownableRunnable {
	public static int ADDRESS_COUNT = 1000;
	
	private final ArrayBlockingQueue<SeedAddressBundle> sink;
	private final int num;

	private boolean isRunning = false;
	
	public WalletGeneratorThread(int num, ArrayBlockingQueue<SeedAddressBundle> sink) {
		this.sink = sink;
		this.num = num;
	}
	
	public void start() {
		this.isRunning = true;
	}

	public void shutdown() {
		this.isRunning = false;
	}

	public void run() {
		System.out.printf("Started Worker Thread %d\n", this.num);
		
		while (this.isRunning) {
			try {
				Wallet wallet = Wallet.createDeterministic(MainNetParams.get(), ScriptType.P2PKH);
				String[] addresses = new String[WalletGeneratorThread.ADDRESS_COUNT];
				
				for (int i = 0; i < WalletGeneratorThread.ADDRESS_COUNT; i++) {
					addresses[i] = wallet.freshReceiveAddress(ScriptType.P2PKH).toString();
				}
				
				String seedPhrase = wallet.getKeyChainSeed().getMnemonicCode().stream().collect(Collectors.joining(" "));
				this.sink.put(new SeedAddressBundle(seedPhrase, addresses));
			} catch (InterruptedException e) {
				System.out.println("Could not add wallet to wallet sink...");
				
				if (this.isRunning) {
					System.out.println("   retrying");
				}
			}
		}
	}
}
